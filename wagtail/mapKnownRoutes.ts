import { Navigation } from "./navigation/types";
import { Footer } from "./footer/types";
import { WagtailPage } from "./pages/types";

export interface KnownRoutesMapping {
  [url: string]: string;
}

export interface KnownPagesSources {
  navigation?: Navigation;
  footer?: Footer;
  page?: WagtailPage;
}

function call<T>(
  data: T | undefined,
  mapper: (data: T) => KnownRoutesMapping,
): KnownRoutesMapping {
  return data !== undefined ? mapper(data) : {};
}

function mapNavigation(data: Navigation): KnownRoutesMapping {
  return data.items.reduce<KnownRoutesMapping>((mapping, item) => {
    mapping[item.urlPath] = item.title;
    return mapping;
  }, {});
}

function mapFooter(data: Footer): KnownRoutesMapping {
  return data.pages.reduce<KnownRoutesMapping>((mapping, item) => {
    mapping[item.urlPath] = item.title;
    return mapping;
  }, {});
}

function mapPage(data: WagtailPage): KnownRoutesMapping {
  return {};
}

export function mapKnownRoutes({
  navigation,
  footer,
  page,
}: KnownPagesSources): KnownRoutesMapping {
  return Object.assign(
    {},
    call(navigation, mapNavigation),
    call(footer, mapFooter),
    call(page, mapPage),
  );
}
