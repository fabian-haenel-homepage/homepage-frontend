export class NotFound extends Error {}

export class NotAvailable extends Error {}
