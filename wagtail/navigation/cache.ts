import { Navigation } from "./types";
import redisClient from "../../lib/redisClient";
import { serverConfiguration } from "../../lib/configuration";

export const REDIS_KEY_NAVIGATION_REPRESENTATION =
  "wagtail::navigation::representation";

export const getNavigationRepresentationFromCache =
  async (): Promise<Navigation> => {
    const raw = await redisClient.get(REDIS_KEY_NAVIGATION_REPRESENTATION);
    if (!raw) {
      throw new Error(); // TODO
    }
    return JSON.parse(raw);
  };

export const cacheNavigationRepresentation = async (
  navigationRepresentation: Navigation,
): Promise<void> => {
  await redisClient.set(
    REDIS_KEY_NAVIGATION_REPRESENTATION,
    JSON.stringify(navigationRepresentation),
    { EX: serverConfiguration.navigationTTL },
  );
};
