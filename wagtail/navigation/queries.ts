import apolloClient from "../lib/apolloClient";
import { gql } from "@apollo/client";
import { Navigation, QueryParams, QueryResult } from "./types";
import { publicConfiguration } from "../../lib/configuration";
import { toNextUrl } from "../lib/helper";

const NAVIGATION_QUERY = gql`
  query ($siteId: ID) {
    site(id: $siteId) {
      siteName
      rootPage {
        id
      }
      pages(showInMenus: true) {
        id
        title
        urlPath
        contentType
      }
    }
  }
`;

export const getNavigationRepresentationFromWagtail =
  async (): Promise<Navigation> => {
    const { data } = await apolloClient.query<QueryResult, QueryParams>({
      query: NAVIGATION_QUERY,
      variables: { siteId: publicConfiguration.wagtailSiteId },
    });

    if (data.site === null) {
      throw new Error(); // TODO
    }

    const { siteName, pages, rootPage } = data.site;

    return {
      siteName,
      items: pages
        .filter(
          ({ id, contentType }) =>
            contentType !== "legal_notice.LegalNoticePage" ||
            id === rootPage.id,
        )
        .map(({ id, urlPath, title }) => ({
          id,
          title,
          urlPath: toNextUrl(urlPath),
        })),
    };
  };
