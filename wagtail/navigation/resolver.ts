import { Navigation } from "./types";
import {
  cacheNavigationRepresentation,
  getNavigationRepresentationFromCache,
} from "./cache";
import { getNavigationRepresentationFromWagtail } from "./queries";

export const resolveNavigationRepresentation =
  async (): Promise<Navigation> => {
    try {
      return await getNavigationRepresentationFromCache();
    } catch {
      const navigationRepresentation =
        await getNavigationRepresentationFromWagtail();

      await cacheNavigationRepresentation(navigationRepresentation);

      return navigationRepresentation;
    }
  };
