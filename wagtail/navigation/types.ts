export interface RootPage {
  id: number;
}

export interface Page {
  id: number;
  title: string;
  urlPath: string;
  contentType: string;
}

export interface Site {
  siteName: string;
  rootPage: RootPage;
  pages: Page[];
}

export interface QueryParams {
  siteId: number;
}

export interface QueryResult {
  site: Site;
}

export interface NavigationItem {
  id: number;
  title: string;
  urlPath: string;
}

export interface Navigation {
  siteName: string;
  items: NavigationItem[];
}
