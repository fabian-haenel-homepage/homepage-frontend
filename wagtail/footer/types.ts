export interface Page {
  id: string;
  title: string;
  urlPath: string;
}

export interface Site {
  siteName: string;
  pages: Page[];
}

export interface QueryParams {
  siteId: number;
}

export interface QueryResult {
  site: Site;
}

export interface Footer extends Site {}
