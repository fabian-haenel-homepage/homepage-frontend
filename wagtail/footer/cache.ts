import { Footer } from "./types";
import redisClient from "../../lib/redisClient";
import { serverConfiguration } from "../../lib/configuration";

export const REDIS_KEY_FOOTER_REPRESENTATION =
  "wagtail::footer::representation";

export const getFooterRepresentationFromCache = async (): Promise<Footer> => {
  const raw = await redisClient.get(REDIS_KEY_FOOTER_REPRESENTATION);
  if (!raw) {
    throw new Error(); // TODO
  }
  return JSON.parse(raw);
};

export const cacheFooterRepresentation = async (
  footerRepresentation: Footer,
): Promise<void> => {
  await redisClient.set(
    REDIS_KEY_FOOTER_REPRESENTATION,
    JSON.stringify(footerRepresentation),
    { EX: serverConfiguration.footerTTL },
  );
};
