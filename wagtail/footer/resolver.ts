import { Footer } from "./types";
import {
  cacheFooterRepresentation,
  getFooterRepresentationFromCache,
} from "./cache";
import { getFooterRepresentationFromWagtail } from "./queries";

export const resolveFooterRepresentation = async (): Promise<Footer> => {
  try {
    return await getFooterRepresentationFromCache();
  } catch {
    const footerRepresentation = await getFooterRepresentationFromWagtail();

    await cacheFooterRepresentation(footerRepresentation);

    return footerRepresentation;
  }
};
