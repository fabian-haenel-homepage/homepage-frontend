import { gql } from "@apollo/client";
import apolloClient from "../lib/apolloClient";
import { publicConfiguration } from "../../lib/configuration";
import { Footer, QueryParams, QueryResult } from "./types";
import { toNextUrl } from "../lib/helper";

const FOOTER_QUERY = gql`
  query ($siteId: ID) {
    site(id: $siteId) {
      siteName
      pages(contentType: "legal_notice.LegalNoticePage") {
        id
        title
        urlPath
      }
    }
  }
`;

export const getFooterRepresentationFromWagtail = async (): Promise<Footer> => {
  const { data } = await apolloClient.query<QueryResult, QueryParams>({
    query: FOOTER_QUERY,
    variables: { siteId: publicConfiguration.wagtailSiteId },
  });

  if (data.site === null) {
    throw new Error(); // TODO
  }
  const { siteName, pages } = data.site;
  return {
    siteName,
    pages: pages.map(({ id, urlPath, title }) => ({
      id,
      title,
      urlPath: toNextUrl(urlPath),
    })),
  };
};
