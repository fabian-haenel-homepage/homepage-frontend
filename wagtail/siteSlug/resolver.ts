import { cacheSiteSlug, getSiteSlugFromCache } from "./cache";
import { getSiteSlugFromWagtail } from "./queries";

export const resolveSiteSlug = async (): Promise<string> => {
  try {
    return await getSiteSlugFromCache();
  } catch {
    const siteSlug = await getSiteSlugFromWagtail();
    await cacheSiteSlug(siteSlug);
    return siteSlug;
  }
};
