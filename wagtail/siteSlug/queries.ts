import { gql } from "@apollo/client";
import apolloClient from "../lib/apolloClient";
import { QueryParams, QueryResult } from "./types";
import { publicConfiguration } from "../../lib/configuration";

const QUERY = gql`
  query ($siteId: ID) {
    site(id: $siteId) {
      rootPage {
        slug
      }
    }
  }
`;

export const getSiteSlugFromWagtail = async (): Promise<string> => {
  const { data } = await apolloClient.query<QueryResult, QueryParams>({
    query: QUERY,
    variables: { siteId: publicConfiguration.wagtailSiteId },
  });
  if (data.site === null) {
    throw new Error(); // TODO
  }
  return data.site.rootPage.slug;
};
