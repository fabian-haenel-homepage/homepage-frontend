export interface RootPage {
  slug: string;
}

export interface Site {
  rootPage: RootPage;
}

export interface QueryResult {
  site: Site;
}

export interface QueryParams {
  siteId: number;
}
