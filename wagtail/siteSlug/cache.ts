import redisClient from "../../lib/redisClient";

export const REDIS_KEY_SITE_SLUG = "wagtail::navigation::siteSlug";

export const REDIS_TTL_SITE_SLUG = 300;

export const getSiteSlugFromCache = async (): Promise<string> => {
  const raw = await redisClient.get(REDIS_KEY_SITE_SLUG);
  if (!raw) {
    throw new Error(); // TODO
  }
  return raw;
};

export const cacheSiteSlug = async (siteSlug: string): Promise<void> => {
  await redisClient.set(REDIS_KEY_SITE_SLUG, siteSlug, {
    EX: REDIS_TTL_SITE_SLUG,
  });
};
