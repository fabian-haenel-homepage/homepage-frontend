import { resolveSiteSlug } from "../siteSlug/resolver";
import { getPageFromWagtail, getPagePreviewFromWagtail } from "./queries";
import { WagtailPage } from "./types";

export const resolvePage = async (slugs: string[]): Promise<WagtailPage> => {
  const siteSlug = await resolveSiteSlug();
  const urlPath = [siteSlug, ...slugs].join("/");
  return await getPageFromWagtail(urlPath);
};

export const resolvePagePreview = getPagePreviewFromWagtail;
