import {
  gql,
  OnSubscriptionDataOptions,
  useSubscription,
} from "@apollo/client";
import {
  WagtailPage,
  QueryParams,
  QueryResult,
  PreviewQueryParams,
} from "./types";
import apolloClient from "../lib/apolloClient";
import { ABOUT_ME_PAGE_FRAGMENT } from "./aboutMe/fragements";
import { LEGAL_NOTICE_FRAGMENT } from "./legalNotice/fragments";
import { NotFound } from "../errors";

const PAGE_QUERY_FRAGMENT = gql`
  ${ABOUT_ME_PAGE_FRAGMENT}
  ${LEGAL_NOTICE_FRAGMENT}

  fragment PageQueryFragment on PageInterface {
    title
    seoTitle
    searchDescription
    contentType

    ... on AboutMePage {
      ...AboutMePageFragment
    }
    ... on LegalNoticePage {
      ...LegalNoticePageFragment
    }
  }
`;

const QUERY = gql`
  ${PAGE_QUERY_FRAGMENT}

  query ($urlPath: String) {
    page(urlPath: $urlPath) {
      ...PageQueryFragment
    }
  }
`;

const PREVIEW_QUERY = gql`
  ${PAGE_QUERY_FRAGMENT}
  query ($contentType: String, $token: String) {
    page(contentType: $contentType, token: $token) {
      ...PageQueryFragment
    }
  }
`;

const PREVIEW_SUBSCRIPTION = gql`
  ${PAGE_QUERY_FRAGMENT}
  subscription ($contentType: String, $token: String) {
    page(contentType: $contentType, token: $token) {
      ...PageQueryFragment
    }
  }
`;

export const getPageFromWagtail = async (
  urlPath: string,
): Promise<WagtailPage> => {
  const { data } = await apolloClient.query<QueryResult, QueryParams>({
    query: QUERY,
    variables: { urlPath },
  });

  if (data.page === null) {
    throw new NotFound();
  }

  return data.page;
};

export const getPagePreviewFromWagtail = async (
  contentType: string,
  token: string,
): Promise<WagtailPage> => {
  const { data } = await apolloClient.query<QueryResult, PreviewQueryParams>({
    query: PREVIEW_QUERY,
    variables: { contentType, token },
  });

  if (data.page === null) {
    throw new NotFound();
  }

  return data.page;
};

export const usePreviewSubscription = (
  contentType: string,
  token: string,
  onSubscriptionData: (options: OnSubscriptionDataOptions<QueryResult>) => any,
) => {
  return useSubscription<QueryResult, PreviewQueryParams>(
    PREVIEW_SUBSCRIPTION,
    {
      variables: { contentType, token },
      client: apolloClient,
      onSubscriptionData,
    },
  );
};
