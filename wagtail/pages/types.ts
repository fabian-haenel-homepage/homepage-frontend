import { AboutMePage } from "./aboutMe/types";
import { LegalNoticePage } from "./legalNotice/types";
import { WagtailPage } from "../../components/pages/WagtailPage";

export type PageType =
  | "about_me.AboutMePage"
  | "legal_notice.LegalNoticePage"
  | "dummy";

export interface WagtailPageBase {
  contentType: PageType;
  title: string;
  seoTitle: string;
}

export interface DummyPage extends WagtailPageBase {
  contentType: "dummy";
}

export type WagtailPage = AboutMePage | LegalNoticePage | DummyPage;

export interface QueryResult {
  page: WagtailPage;
}

export interface QueryParams {
  urlPath: string;
}

export interface PreviewQueryParams {
  contentType: string;
  token: string;
}
