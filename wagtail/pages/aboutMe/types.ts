import { WagtailPageBase } from "../types";

export interface Image {
  url: string;
  width: number;
  height: number;
}

export interface CharBlock {
  value: string;
}

export interface TextBlock {
  value: string;
}

export interface LinkBlock {
  label: string;
  url: string;
}

export interface ImageBlock {
  image: Image;
}

export interface Experience {
  startDate: string;
  endDate: string;
  role: string;
  icon: Image;
  tags: string[];
  company: string;
}

export type TechnologyCategory =
  | "Programming Language"
  | "Framework"
  | "Database"
  | "API"
  | "DevOps";

export interface TechnologyLogo extends Image {
  width: number;
  height: number;
}

export interface Technology {
  name: string;
  category: TechnologyCategory;
  logo: TechnologyLogo | null;
}

export interface Education {
  startDate: string;
  endDate: string;
  title: string;
  graduationTopic: string;
  institute: string;
  instituteLogo: Image;
}

export enum LanguageLevel {
  NONE = 0,
  BASIC = 5,
  ADVANCED = 10,
  FLUENT = 15,
  NATIVE = 20,
}

export interface Language {
  language: string;
  level: LanguageLevel;
}

export type DescriptionBlockTypes =
  | "SubTitle"
  | "Paragraph"
  | "Annotation"
  | "Link"
  | "Image";

export interface DescriptionBlockBase {
  field: DescriptionBlockTypes;
}

export interface DescriptionSubTitle extends DescriptionBlockBase, CharBlock {
  field: "SubTitle";
}

export interface DescriptionParagraph extends DescriptionBlockBase, TextBlock {
  field: "Paragraph";
}

export interface DescriptionAnnotation extends DescriptionBlockBase, CharBlock {
  field: "Annotation";
}

export interface DescriptionLink extends DescriptionBlockBase, LinkBlock {
  field: "Link";
}
export interface DescriptionImage extends DescriptionBlockBase, ImageBlock {
  field: "Image";
}
export type DescriptionBlock =
  | DescriptionSubTitle
  | DescriptionParagraph
  | DescriptionAnnotation
  | DescriptionLink
  | DescriptionImage;

export interface AboutMePage extends WagtailPageBase {
  contentType: "about_me.AboutMePage";
  titleImage: Image | null;
  description: DescriptionBlock[];
  experiences: Experience[];
  technologyComment: string;
  technologies: Technology[];
  education: Education[];
  languages: Language[];
}
