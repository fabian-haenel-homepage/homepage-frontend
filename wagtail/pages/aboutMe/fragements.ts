import { gql } from "@apollo/client";

export const ABOUT_ME_PAGE_FRAGMENT = gql`
  fragment AboutMePageFragment on AboutMePage {
    titleImage {
      url
      width
      height
    }
    description {
      field
      ... on CharBlock {
        value
      }
      ... on TextBlock {
        value
      }
      ... on LinkBlock {
        label
        url
      }
      ... on ImageChooserBlock {
        image {
          url
          width
          height
        }
      }
    }
    experiences {
      ... on ExperienceBlock {
        startDate
        endDate
        role
        icon {
          url
          width
          height
        }
        tags
        company
      }
    }
    technologyComment
    technologies {
      name
      category
      logo {
        url
        width
        height
      }
    }
    education {
      ... on EducationBlock {
        startDate
        endDate
        title
        graduationTopic
        institute
        instituteLogo {
          url
          width
          height
        }
      }
    }
    languages {
      ... on LanguageBlock {
        language
        level
      }
    }
  }
`;
