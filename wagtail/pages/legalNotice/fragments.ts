import { gql } from "@apollo/client";

export const LEGAL_NOTICE_FRAGMENT = gql`
  fragment LegalNoticePageFragment on LegalNoticePage {
    fullName
    street
    houseNumber
    postalCode
    city
    country
    phoneNumber
    email
    content {
      field
      ... on CharBlock {
        value
      }
      ... on TextBlock {
        value
      }
      ... on LinkBlock {
        label
        url
      }
    }
  }
`;
