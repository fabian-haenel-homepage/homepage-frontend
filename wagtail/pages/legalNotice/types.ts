import { WagtailPageBase } from "../types";

export interface CharBlock {
  value: string;
}

export interface TextBlock {
  value: string;
}

export interface LinkBlock {
  label: string;
  url: string;
}

export type ContentBlockType =
  | "H2"
  | "H3"
  | "Paragraph"
  | "Annotation"
  | "Link";

export interface ContentBlockBase {
  field: ContentBlockType;
}

export interface H2 extends ContentBlockBase, CharBlock {
  field: "H2";
}
export interface H3 extends ContentBlockBase, CharBlock {
  field: "H3";
}
export interface Paragraph extends ContentBlockBase, TextBlock {
  field: "Paragraph";
}
export interface Annotation extends ContentBlockBase, TextBlock {
  field: "Annotation";
}
export interface Link extends ContentBlockBase, LinkBlock {
  field: "Link";
}

export type ContentBlock = H2 | H3 | Paragraph | Annotation | Link;

export interface LegalNoticePage extends WagtailPageBase {
  contentType: "legal_notice.LegalNoticePage";
  fullName: string;
  street: string;
  houseNumber: string;
  postalCode: string;
  city: string;
  country: string;
  phoneNumber: string;
  email: string;
  content: ContentBlock[];
}
