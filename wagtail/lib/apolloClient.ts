import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  InMemoryCache,
  split,
} from "@apollo/client";
import { publicConfiguration } from "../../lib/configuration";
import { getMainDefinition } from "@apollo/client/utilities";
import { GraphQLWsLink } from "@apollo/client/link/subscriptions";
import { createClient } from "graphql-ws";

const httpLink = new HttpLink({
  uri: `${publicConfiguration.wagtailBaseUrl}/api/graphql/`,
});

let link: ApolloLink = httpLink;
if (typeof window !== "undefined") {
  const wsLink = new GraphQLWsLink(
    createClient({
      url: `${publicConfiguration.wagtailWebsocketBaseUrl}/subscriptions`,
    }),
  );

  link = split(
    ({ query }) => {
      const definition = getMainDefinition(query);
      return (
        definition.kind === "OperationDefinition" &&
        definition.operation === "subscription"
      );
    },
    wsLink,
    httpLink,
  );
}

const apolloClient = new ApolloClient({
  link,
  cache: new InMemoryCache(),
  defaultOptions: {
    query: {
      fetchPolicy: "no-cache",
      errorPolicy: "all",
    },
    watchQuery: {
      fetchPolicy: "no-cache",
      errorPolicy: "ignore",
    },
  },
});

export default apolloClient;
