export interface PaginationMeta {
  total_count: number;
}

export interface PaginatedResponse<TItems> {
  meta: PaginationMeta;
  items: TItems[];
}
