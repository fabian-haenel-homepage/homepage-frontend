export function toNextUrl(urlPath: string) {
  return urlPath
    .split("/")
    .filter((segment) => segment !== "")
    .slice(1)
    .join("/");
}
