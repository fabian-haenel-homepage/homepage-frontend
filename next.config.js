/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  images: {
    domains: process.env.IMAGE_DOMAINS?.split(","),
  },
  serverRuntimeConfig: {
    redisUrl: process.env.REDIS_URL,
    pageTTL: process.env.PAGE_TTL,
    navigationTTL: process.env.NAVIGATION_TTL,
    footerTTL: process.env.FOOTER_TTL,
  },
  publicRuntimeConfig: {
    wagtailBaseUrl: process.env.WAGTAIL_BASE_URL,
    wagtailWebsocketBaseUrl: process.env.WAGTAIL_WEBSOCKET_BASE_URL,
    wagtailSiteId: process.env.WAGTAIL_SITE_ID,
  },
};
