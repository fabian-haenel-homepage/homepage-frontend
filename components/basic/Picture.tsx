import React, { useState } from "react";
import Image from "next/image";
import { ClassNames } from "../lib/types";
import Icon from "@mdi/react";
import { mdiImage, mdiImageOff } from "@mdi/js";
import { cn, insert } from "../lib/classNames";

export const TRANSPARENT_PIXEL_BASE64 =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";

export interface PictureProps {
  src: string;
  width: number;
  height: number;
  className?: ClassNames;
}

export const Picture: React.FC<PictureProps> = ({
  src,
  width,
  height,
  className,
}) => {
  const [isError, setIsError] = useState(false);
  const classNames = ["block", "relative", "w-full", "h-full"];
  const placeholderIcon = isError ? mdiImageOff : mdiImage;

  insert(classNames, className);

  return (
    <div className={cn(classNames)}>
      {isError ? (
        <div
          className={cn(
            "absolute",
            "flex",
            "items-center",
            "justify-center",
            "h-full",
            "w-full",
            "bg-gradient-radial",
            "from-gray-600",
            "to-gray-700",
            "rounded-3xl",
          )}
        >
          <Icon className="w-3/4 h-3/4 text-gray-400" path={placeholderIcon} />
        </div>
      ) : null}

      <Image
        className={isError ? "none" : "w-full h-full"}
        src={src}
        alt=" "
        width={width}
        height={height}
        layout="responsive"
        onError={() => setIsError(true)}
        onLoadingComplete={() => setIsError(false)}
        placeholder="blur"
        blurDataURL={TRANSPARENT_PIXEL_BASE64}
      />
    </div>
  );
};
