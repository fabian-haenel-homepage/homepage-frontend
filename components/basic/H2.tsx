import React from "react";
import { Alignment, ClassNames } from "../lib/types";
import { insert, cn } from "../lib/classNames";
import { useAlignment } from "./Alignment";

export interface H2Props {
  align?: Alignment;
  className?: ClassNames;
  children?: string | number;
}

export const H2: React.FC<H2Props> = ({ align, className, children }) => {
  const defaultAlignment = useAlignment();

  const classNames = [
    "text-primary-500",
    "text-2xl",
    "font-normal",
    "text-center",
  ];

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push("sm:text-right");
      break;
    case "left":
    default:
      classNames.push("sm:text-left");
  }
  insert(classNames, className);

  return <h3 className={cn(classNames)}>{children}</h3>;
};
