import React from "react";

export interface IfProps {
  condition: boolean | string | number | null | unknown[];
  children?: React.ReactNode | string | number;
}

// TODO typing needs to be disabled for the time being:
//   https://github.com/microsoft/TypeScript/issues/21699
//   https://github.com/DefinitelyTyped/DefinitelyTyped/issues/18051#issuecomment-819773205
// @ts-ignore
export const If: React.FC<IfProps> = ({ condition, children = null }) => {
  return (condition instanceof Array && condition.length === 0) || !condition
    ? null
    : children;
};
