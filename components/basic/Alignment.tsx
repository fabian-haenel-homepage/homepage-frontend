import React, { useContext } from "react";
import { Alignment } from "../lib/types";

const Context = React.createContext<Alignment>("left");

export interface AlignmentContextProps {
  alignment: Alignment;
  children?: React.ReactNode | string | number;
}

export const useAlignment = (): Alignment => useContext(Context);

export const AlignmentContext: React.FC<AlignmentContextProps> = ({
  alignment,
  children,
}) => <Context.Provider value={alignment}>{children}</Context.Provider>;
