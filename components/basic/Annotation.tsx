import React from "react";
import { Alignment, ClassNames } from "../lib/types";
import { insert, cn } from "../lib/classNames";
import { useAlignment } from "./Alignment";

interface AnnotationProps {
  align?: Alignment;
  className?: ClassNames;
  children?: string | number;
}

export const Annotation: React.FC<AnnotationProps> = ({
  align,
  className,
  children,
}) => {
  const defaultAlignment = useAlignment();

  const classNames = [
    "text-gray-500",
    "whitespace-pre-wrap",
    "text-xs",
    "text-center",
    "self-center",
  ];

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push("sm:text-right", "sm:self-end");
      break;
    case "left":
    default:
      classNames.push("sm:text-left", "sm:self-start");
  }
  insert(classNames, className);
  return <span className={cn(classNames)}>{children}</span>;
};
