import React from "react";
import { Alignment, ClassNames } from "../lib/types";
import { insert, cn } from "../lib/classNames";
import { useAlignment } from "./Alignment";

export interface H4Props {
  align?: Alignment;
  className?: ClassNames;
  children?: string | number;
}

export const H4: React.FC<H4Props> = ({ align, className, children }) => {
  const defaultAlignment = useAlignment();

  const classNames = ["text-gray-400", "text-lg", "font-normal", "text-center"];

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push("sm:text-right");
      break;
    case "left":
    default:
      classNames.push("sm:text-left");
  }
  insert(classNames, className);

  return <h4 className={cn(classNames)}>{children}</h4>;
};
