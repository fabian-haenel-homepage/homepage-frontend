import React from "react";
import { Alignment, ClassNames } from "../lib/types";
import { insert, cn } from "../lib/classNames";
import { useAlignment } from "./Alignment";

export interface H6Props {
  align?: Alignment;
  className?: ClassNames;
  children?: string | number;
}

export const H6: React.FC<H6Props> = ({ align, className, children }) => {
  const defaultAlignment = useAlignment();

  const classNames = ["text-lg", "font-normal", "text-center"];

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push("sm:text-right");
      break;
    case "left":
    default:
      classNames.push("sm:text-left");
  }
  insert(classNames, className);

  return <h6 className={cn(classNames)}>{children}</h6>;
};
