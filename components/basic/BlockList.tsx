import React from "react";
import { Alignment, ClassNames } from "../lib/types";
import { useAlignment } from "./Alignment";
import { insert, cn } from "../lib/classNames";

interface BlockListProps {
  align?: Alignment;
  className?: ClassNames;
  children?: React.ReactNode;
}

export const BlockList: React.FC<BlockListProps> = ({
  align,
  className,
  children,
}) => {
  const defaultAlignment = useAlignment();

  const classNames = ["flex", "items-left", "gap-8"];

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push("flex-col-reverse");
      break;
    case "left":
    default:
      classNames.push("flex-col");
  }
  insert(classNames, className);

  return <ul className={cn(classNames)}>{children}</ul>;
};
