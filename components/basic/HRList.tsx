import React from "react";
import { ClassNames } from "../lib/types";
import { insert, cn } from "../lib/classNames";
import { HR } from "./HR";

export interface HorizontalRuleListProps {
  className?: ClassNames;
  children?: React.ReactNode;
}

export const HRList: React.FC<HorizontalRuleListProps> = ({
  className,
  children,
}) => {
  const classNames = ["flex", "gap-4", "flex-col"];
  insert(classNames, className);
  return (
    <div className={cn(classNames)}>
      {React.Children.toArray(children).map((child, index) => (
        <React.Fragment key={index}>
          {index > 0 ? <HR /> : null}
          {child}
        </React.Fragment>
      ))}
    </div>
  );
};
