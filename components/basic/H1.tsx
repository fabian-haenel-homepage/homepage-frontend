import React from "react";
import Icon from "@mdi/react";
import { Alignment } from "../lib/types";
import { cn } from "../lib/classNames";
import { useAlignment } from "./Alignment";

export interface H1Props {
  align?: Alignment;
  iconPath: string;
  children?: string | number;
}

export const H1: React.FC<H1Props> = ({ align, iconPath, children }) => {
  const defaultAlignment = useAlignment();

  const classNames = [
    "flex",
    "items-center",
    "p-4",
    "my-8",
    "w-full",
    "sm:w-96",
    "from-gray-700",
  ];

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push(
        "flex-row-reverse",
        "bg-gradient-to-l",
        "rounded-r-sm",
        "self-end",
        "text-right",
      );
      break;
    case "left":
    default:
      classNames.push(
        "flex-row",
        "bg-gradient-to-r",
        "rounded-l-sm",
        "self-start",
        "text-left",
      );
  }

  return (
    <h1 className={cn(classNames)}>
      <Icon
        className="h-12 text-primary-500 px-2 flex-grow-0"
        path={iconPath}
      />
      <div className="font-bold text-gray-300 text-3xl select-none">
        {children}
      </div>
    </h1>
  );
};
