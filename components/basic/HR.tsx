import React from "react";
import { ClassNames } from "../lib/types";
import { cn } from "../lib/classNames";

export interface HRProps {
  className?: ClassNames;
}

export const HR: React.FC<HRProps> = ({ className }) => (
  <hr className={cn("border-gray-700", "w-full", className)} />
);
