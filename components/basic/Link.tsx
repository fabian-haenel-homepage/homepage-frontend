import React from "react";
import { Alignment, ClassNames } from "../lib/types";
import { insert, cn } from "../lib/classNames";
import { useAlignment } from "./Alignment";
import Icon from "@mdi/react";
import { mdiLink } from "@mdi/js";

interface LinkProps {
  href: string;
  align?: Alignment;
  className?: ClassNames;
  children?: string | number;
}

export const Link: React.FC<LinkProps> = ({
  href,
  align,
  className,
  children,
}) => {
  const defaultAlignment = useAlignment();

  const aClassNames = [
    "flex",
    "text-primary-500",
    "items-center",
    "text-xl",
    "self-center",
  ];
  const iconClassNames = ["flex-grow-0", "px-1", "h-6"];

  switch (align ?? defaultAlignment) {
    case "right":
      aClassNames.push("text-right", "sm:self-end", "flex-row-reverse");
      break;
    case "left":
    default:
      aClassNames.push("text-left", "sm:self-start", "flex-row");
  }
  insert(aClassNames, className);
  return (
    <a className={cn(aClassNames)} href={href}>
      <Icon className={cn(iconClassNames)} path={mdiLink} />
      <div>{children}</div>
    </a>
  );
};
