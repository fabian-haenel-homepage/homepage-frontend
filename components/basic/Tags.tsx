import React from "react";
import { useAlignment } from "./Alignment";
import { insert, cn } from "../lib/classNames";
import { Alignment, ClassNames } from "../lib/types";

interface TagsProps {
  align?: Alignment;
  className?: ClassNames;
  items: string[];
}

export const Tags: React.FC<TagsProps> = ({ align, className, items }) => {
  const defaultAlignment = useAlignment();

  const classNames = [
    "flex",
    "gap-1",
    "py-2",
    "flex-wrap",
    "content-center",
    "font-mono",
    "justify-center",
    "flex-row",
  ];

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push("sm:flex-row-reverse", "sm:justify-start");
      break;
    case "left":
    default:
      classNames.push("sm:justify-end");
  }
  insert(classNames, className);

  return (
    <ul className={cn(classNames)}>
      {items?.map((value, index) => (
        <React.Fragment key={index}>
          {index !== 0 ? (
            <li className="text-primary-500 text-xl select-none leading-5">
              &bull;
            </li>
          ) : null}
          <li className="text-gray-500 text-xs select-all leading-5">
            {value}
          </li>
        </React.Fragment>
      ))}
    </ul>
  );
};
