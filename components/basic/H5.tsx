import React from "react";
import { Alignment, ClassNames } from "../lib/types";
import { insert, cn } from "../lib/classNames";
import { useAlignment } from "./Alignment";

export interface H5Props {
  align?: Alignment;
  className?: ClassNames;
  children?: string | number;
}

export const H5: React.FC<H5Props> = ({ align, className, children }) => {
  const defaultAlignment = useAlignment();

  const classNames = [
    "text-primary-500",
    "text-lg",
    "font-normal",
    "text-center",
  ];

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push("sm:text-right");
      break;
    case "left":
    default:
      classNames.push("sm:text-left");
  }
  insert(classNames, className);

  return <h5 className={cn(classNames)}>{children}</h5>;
};
