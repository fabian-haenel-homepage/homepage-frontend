import React from "react";
import { Alignment, ClassNames } from "../lib/types";
import { useAlignment } from "./Alignment";
import { insert, cn } from "../lib/classNames";
import English from "../../i18n/components/Range/en.json";

interface Localization {
  presentLabel: string;
}

export interface RangeProps {
  from: string;
  to: string | null;
  align?: Alignment;
  className?: ClassNames;
}

const i18n = (locale: string): Localization => {
  switch (locale) {
    case "en":
      return English;
    default:
      return English;
  }
};

export const Range: React.FC<RangeProps> = ({ from, to, align, className }) => {
  const localization = i18n("en"); // TODO use next.js localization feature
  const defaultAlignment = useAlignment();

  const classNames = [
    "text-gray-400",
    "text-sm",
    "font-normal",
    "text-center",
    "self-center",
  ];

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push("sm:text-right", "sm:self-end");
      break;
    case "left":
    default:
      classNames.push("sm:text-left", "sm:self-start");
  }
  insert(classNames, className);

  return (
    <div className={cn(classNames)}>
      {from} &#8210; {to ?? localization.presentLabel}
    </div>
  );
};
