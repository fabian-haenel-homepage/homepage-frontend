import React from "react";
import { Alignment, ClassNames, MobileAlignment } from "../lib/types";
import { insert, cn } from "../lib/classNames";
import { useAlignment } from "./Alignment";

interface ParagraphProps {
  align?: Alignment;
  alignMobile?: MobileAlignment;
  className?: ClassNames;
  children?: string | number;
}

export const Paragraph: React.FC<ParagraphProps> = ({
  align,
  alignMobile = "center",
  className,
  children,
}) => {
  const defaultAlignment = useAlignment();

  const classNames = ["text-gray-400", "whitespace-pre-wrap", "font-medium"];

  if (alignMobile == "center") {
    classNames.push("text-justify", "self-center");
  }

  switch (align ?? defaultAlignment) {
    case "right":
      classNames.push(
        alignMobile == "default"
          ? "text-right self-end"
          : "sm:text-right sm:self-end",
      );
      break;
    case "left":
    default:
      classNames.push(
        alignMobile == "default"
          ? "text-left self-start"
          : "sm:text-left sm:self-start",
      );
  }
  insert(classNames, className);
  return <p className={cn(classNames)}>{children}</p>;
};
