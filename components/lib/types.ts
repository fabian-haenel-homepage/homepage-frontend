export type Alignment = "left" | "right";
export type MobileAlignment = "default" | "center";

export type ClassNames = string | string[];
