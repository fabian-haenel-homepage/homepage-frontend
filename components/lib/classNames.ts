import { ClassNames } from "./types";

export function insert(target: string[], source?: ClassNames) {
  if (typeof source === "string") {
    target.push(source);
  }

  if (Array.isArray(source)) {
    target.push(...source);
  }
}

export function cn(...classNames: (ClassNames | undefined)[]): string {
  return classNames
    .flatMap((value) => {
      if (typeof value === "string") {
        return value;
      }

      if (Array.isArray(value)) {
        return value.join(" ");
      }
      return "";
    })
    .join(" ");
}
