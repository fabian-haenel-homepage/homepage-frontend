import React from "react";
import {
  PageType,
  WagtailPage as WagtailPageProps,
} from "../../wagtail/pages/types";
import { AboutMe, AboutMeFC } from "./AboutMe";
import { Fallback, FallbackFC } from "./Fallback";
import { LegalNotice, LegalNoticeFC } from "./LegalNotice";

export type WagtailPageFC = AboutMeFC | FallbackFC | LegalNoticeFC;

export const getPageFromType = (pageType: PageType): WagtailPageFC => {
  switch (pageType) {
    case "about_me.AboutMePage":
      return AboutMe;
    case "legal_notice.LegalNoticePage":
      return LegalNotice;
    default:
      return Fallback;
  }
};

export const WagtailPage: React.FC<WagtailPageProps> = (props) => {
  const Page = getPageFromType(props.contentType);
  // @ts-ignore
  return <Page {...props} />;
};
