import React from "react";
import { Experience as ExperienceProps } from "../../../wagtail/pages/aboutMe/types";
import { H2 } from "../../basic/H2";
import { H3 } from "../../basic/H3";
import { Range } from "../../basic/Range";
import { Tags } from "../../basic/Tags";
import { useAlignment } from "../../basic/Alignment";
import { cn } from "../../lib/classNames";
import { Picture } from "../../basic/Picture";

export const Experience: React.FC<ExperienceProps> = ({
  company,
  tags,
  icon,
  role,
  startDate,
  endDate,
}) => {
  const defaultAlignment = useAlignment();

  const classNames = ["flex", "items-center", "gap-6", "flex-col"];

  switch (defaultAlignment) {
    case "right":
      classNames.push("sm:flex-row-reverse");
      break;
    case "left":
    default:
      classNames.push("sm:flex-row");
      break;
  }

  return (
    <li className={cn(classNames)}>
      <div
        className={cn(
          "relative",
          "rounded-3xl",
          "overflow-hidden",
          "flex-none",
          "bg-gradient-radial",
          "from-gray-600",
          "to-gray-700",
          "w-36",
          "h-36",
        )}
      >
        <Picture src={icon.url} width={icon.width} height={icon.height} />
      </div>
      <div className="flex flex-col flex-grow">
        <H2>{role}</H2>
        <H3>{company}</H3>
        <Range className="pt-1" from={startDate} to={endDate} />
        <Tags items={tags} />
      </div>
    </li>
  );
};
