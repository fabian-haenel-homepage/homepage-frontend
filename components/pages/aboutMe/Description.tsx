import React from "react";
import {
  DescriptionAnnotation,
  DescriptionBlock,
  DescriptionBlockTypes,
  DescriptionImage,
  DescriptionLink,
  DescriptionParagraph,
  DescriptionSubTitle,
} from "../../../wagtail/pages/aboutMe/types";
import { H2 } from "../../basic/H2";
import { Paragraph } from "../../basic/Paragraph";
import { Annotation } from "../../basic/Annotation";
import { Link } from "../../basic/Link";
import { insert, cn } from "../../lib/classNames";
import { ClassNames } from "../../lib/types";
import { Picture } from "../../basic/Picture";

interface DescriptionProps {
  className?: ClassNames;
  blocks: DescriptionBlock[];
}

const SubTitleBlock: React.FC<DescriptionSubTitle> = ({ value }) => (
  <H2>{value}</H2>
);

const AnnotationBlock: React.FC<DescriptionAnnotation> = ({ value }) => (
  <Annotation>{value}</Annotation>
);

const ParagraphBlock: React.FC<DescriptionParagraph> = ({ value }) => (
  <Paragraph>{value}</Paragraph>
);

const LinkBlock: React.FC<DescriptionLink> = ({ url, label }) => (
  <Link href={url}>{label}</Link>
);

const ImageBlock: React.FC<DescriptionImage> = ({
  image: { url, width, height },
}) => <Picture src={url} width={width} height={height} />;

const getBlockComponentForField = (field: DescriptionBlockTypes) => {
  switch (field) {
    case "SubTitle":
      return SubTitleBlock;
    case "Paragraph":
      return ParagraphBlock;
    case "Annotation":
      return AnnotationBlock;
    case "Link":
      return LinkBlock;
    case "Image":
      return ImageBlock;
    default:
      // eslint-disable-next-line react/display-name
      return () => <></>;
  }
};

export const Description: React.FC<DescriptionProps> = ({
  className,
  blocks,
}) => {
  const classNames = ["flex", "gap-4", "flex-col"];
  insert(classNames, className);
  return (
    <div className={cn(classNames)}>
      {blocks.map((block, index) => {
        const Block = getBlockComponentForField(block.field);
        // @ts-ignore
        return <Block key={index} {...block} />;
      })}
    </div>
  );
};
