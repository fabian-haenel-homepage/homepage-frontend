import React from "react";
import {
  Language as LanguageProps,
  LanguageLevel,
} from "../../../wagtail/pages/aboutMe/types";
import English from "../../../i18n/wagtail/aboutMe/languages/en.json";
import Icon from "@mdi/react";
import { mdiBookOpenVariant } from "@mdi/js";
import { H5 } from "../../basic/H5";
import { useAlignment } from "../../basic/Alignment";
import { cn } from "../../lib/classNames";
import { H6 } from "../../basic/H6";

interface Localization {
  NoneLabel: string;
  BasicLabel: string;
  AdvancedLabel: string;
  FluentLabel: string;
  NativeLabel: string;
}

const i18n = (locale: string): Localization => {
  switch (locale) {
    case "en":
      return English;
    default:
      return English;
  }
};

export const Language: React.FC<LanguageProps> = ({ language, level }) => {
  const defaultAlignment = useAlignment();
  const localization = i18n("en"); // TODO use next.js localization feature
  const itemClassNames = ["flex", "items-center", "gap-4", "flex-col"];
  const booksClassNames = ["flex", "items-center", "gap-2"];

  let levelColorClassName = "";
  let label = null;
  let levelCount = 0;

  switch (defaultAlignment) {
    case "right":
      itemClassNames.push("sm:flex-row-reverse");
      booksClassNames.push("flex-row-reverse");
      break;
    case "left":
    default:
      itemClassNames.push("sm:flex-row");
      booksClassNames.push("flex-row");
  }

  switch (level) {
    case LanguageLevel.NATIVE:
      label = localization.NativeLabel;
      levelColorClassName = "text-language-native";
      levelCount = 4;
      break;
    case LanguageLevel.FLUENT:
      label = localization.FluentLabel;
      levelColorClassName = "text-language-fluent";
      levelCount = 3;
      break;
    case LanguageLevel.ADVANCED:
      label = localization.AdvancedLabel;
      levelColorClassName = "text-language-advanced";
      levelCount = 2;
      break;
    case LanguageLevel.BASIC:
      label = localization.BasicLabel;
      levelColorClassName = "text-language-basic";
      levelCount = 1;
      break;
    case LanguageLevel.NONE:
      label = localization.NoneLabel;
      levelColorClassName = "text-language-none";
      levelCount = 0;
      break;
  }

  return (
    <li className={cn(itemClassNames)}>
      <H5 className="w-32">{language}</H5>
      <div className={cn(booksClassNames)}>
        {Array.from({ length: 5 }, (_, index) => (
          <Icon
            key={index}
            path={mdiBookOpenVariant}
            className={[
              "w-8",
              "h-8",
              "rounded",
              index > levelCount ? "text-gray-600" : levelColorClassName,
            ].join(" ")}
          />
        ))}
      </div>
      <H6 className={cn("font-bold", levelColorClassName)}>{label}</H6>
    </li>
  );
};
