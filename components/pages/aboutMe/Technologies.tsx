import React from "react";
import {
  Technology,
  TechnologyCategory,
  TechnologyLogo,
} from "../../../wagtail/pages/aboutMe/types";
import English from "../../../i18n/wagtail/aboutMe/technologies/en.json";
import { useAlignment } from "../../basic/Alignment";
import { cn } from "../../lib/classNames";
import { H5 } from "../../basic/H5";
import { HRList } from "../../basic/HRList";
import { Picture } from "../../basic/Picture";

export interface TechnologiesProps {
  items: Technology[];
  className: string;
}

interface TechnologyCategoryProps {
  category: TechnologyCategory;
  items: Technology[];
}

interface Localization {
  programmingLanguageCategoryLabel: string;
  frameworkCategoryLabel: string;
  databaseCategoryLabel: string;
  APICategoryLabel: string;
  DevOpsCategoryLabel: string;
}

type TechnologiesByCategoryMapping = {
  [key in TechnologyCategory]?: Technology[];
};

const i18n = (locale: string): Localization => {
  switch (locale) {
    case "en":
      return English;
    default:
      return English;
  }
};

const mapTechnologiesByCategory = (
  technologies: Technology[],
): TechnologiesByCategoryMapping => {
  return technologies.reduce<TechnologiesByCategoryMapping>(
    (map, technology) => {
      if (!(technology.category in map)) {
        map[technology.category] = [];
      }
      map[technology.category]?.push(technology);
      return map;
    },
    {},
  );
};

const Logo: React.FC<TechnologyLogo> = ({ url, width, height }) => {
  const step = 3;
  const ratio = Math.ceil((width / height) * step);
  const classNames = [
    "block",
    "relative",
    "rounded-xl",
    "overflow-hidden",
    "h-12",
  ];

  const sizes = [
    "w-12",
    "w-16",
    "w-20",
    "w-24",
    "w-28",
    "w-32",
    "w-36",
    "w-40",
    "w-44",
    "w-48",
    "w-52",
    "w-56",
    "w-60",
  ];
  const sizeIndex = ratio - step;
  if (sizeIndex >= 0 && sizeIndex < sizes.length) {
    classNames.push(sizes[sizeIndex]);
  } else {
    classNames.push("hidden");
  }

  return (
    <li className={classNames.join(" ")}>
      <Picture src={url} width={width} height={height} />
      <div className="absolute text-primary-500 z-40">{ratio}</div>
    </li>
  );
};

const Category: React.FC<TechnologyCategoryProps> = ({ category, items }) => {
  const defaultAlignment = useAlignment();

  const localization = i18n("en"); // TODO use next.js localization feature

  const classNames = ["flex", "items-center", "gap-8", "flex-col"];
  const listClassNames = ["flex", "items-center", "gap-4", "flex-wrap"];

  switch (defaultAlignment) {
    case "right":
      classNames.push("sm:flex-row-reverse");
      listClassNames.push("flex-row-reverse");
      break;
    case "left":
    default:
      classNames.push("sm:flex-row");
      listClassNames.push("flex-row");
      break;
  }

  let categoryLabel = "";
  switch (category) {
    case "Programming Language":
      categoryLabel = localization.programmingLanguageCategoryLabel;
      break;
    case "Framework":
      categoryLabel = localization.frameworkCategoryLabel;
      break;
    case "Database":
      categoryLabel = localization.databaseCategoryLabel;
      break;
    case "API":
      categoryLabel = localization.APICategoryLabel;
      break;
    case "DevOps":
      categoryLabel = localization.DevOpsCategoryLabel;
      break;
  }

  return (
    <div className={cn(classNames)}>
      <H5 className="w-32">{categoryLabel}</H5>
      <ul className={cn(listClassNames)}>
        {items.map(({ name, logo }, index) =>
          logo ? <Logo key={index} {...logo} /> : null,
        )}
      </ul>
    </div>
  );
};

export const Technologies: React.FC<TechnologiesProps> = ({
  items,
  className,
}) => (
  <HRList className={className}>
    {Object.entries<Technology[]>(mapTechnologiesByCategory(items)).map(
      ([category, technologies]) => (
        <Category
          key={category}
          category={category as TechnologyCategory}
          items={technologies}
        />
      ),
    )}
  </HRList>
);
