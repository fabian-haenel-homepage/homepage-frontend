import { Education as EducationProps } from "../../../wagtail/pages/aboutMe/types";
import React from "react";
import { useAlignment } from "../../basic/Alignment";
import { cn } from "../../lib/classNames";
import { H2 } from "../../basic/H2";
import { H4 } from "../../basic/H4";
import { H3 } from "../../basic/H3";
import { Range } from "../../basic/Range";
import { Picture } from "../../basic/Picture";

export const Education: React.FC<EducationProps> = ({
  title,
  startDate,
  endDate,
  graduationTopic,
  institute,
  instituteLogo,
}) => {
  const defaultAlignment = useAlignment();

  const classNames = ["flex", "items-center", "gap-6", "flex-col"];

  switch (defaultAlignment) {
    case "right":
      classNames.push("sm:flex-row-reverse");
      break;
    case "left":
    default:
      classNames.push("sm:flex-row");
      break;
  }

  return (
    <li className={cn(classNames)}>
      <div
        className={cn(
          "relative",
          "w-36",
          "h-36",
          "rounded-3xl",
          "overflow-hidden",
          "flex-none",
          "bg-gradient-radial",
          "from-gray-600",
          "to-gray-700",
        )}
      >
        <Picture
          src={instituteLogo.url}
          width={instituteLogo.width}
          height={instituteLogo.height}
        />
      </div>
      <div className="flex flex-col flex-grow">
        <H2>{title}</H2>
        <H4>{graduationTopic}</H4>
        <H3>{institute}</H3>
        <Range className="pt-1" from={startDate} to={endDate} />
      </div>
    </li>
  );
};
