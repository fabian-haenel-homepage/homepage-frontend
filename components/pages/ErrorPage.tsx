import English from "../../i18n/components/Error/en.json";
import {
  mdiAccountLock,
  mdiAccountQuestion,
  mdiBug,
  mdiCheckAll,
  mdiExclamationThick,
  mdiHarddiskRemove,
  mdiLanCheck,
  mdiLanDisconnect,
  mdiLinkOff,
  mdiLinkVariant,
  mdiReload,
  mdiScale,
} from "@mdi/js";
import { Site } from "../common/Site";
import Head from "next/head";
import Icon from "@mdi/react";
import { cn } from "../lib/classNames";
import React from "react";
import { HTTP_STATUS_CODES } from "../../lib/common";

export interface ErrorProps {
  statusCode?: number;
}

interface Localization {
  defaultTitle: string;
  returnHomeLabel: string;
}

const i18n = (locale: string): Localization => {
  switch (locale) {
    case "en":
      return English;
    default:
      return English;
  }
};

export const ErrorPage: React.FC<ErrorProps> = ({ statusCode = -1 }) => {
  const localization = i18n("en"); // TODO

  let iconPath = mdiBug;
  let title = HTTP_STATUS_CODES[statusCode] ?? localization.defaultTitle;
  switch (statusCode) {
  }
  switch (true) {
    case statusCode >= 100 && statusCode < 200:
      iconPath = mdiLanCheck;
      break;
    case statusCode >= 200 && statusCode < 300:
      iconPath = mdiCheckAll;
      break;
    case statusCode >= 300 && statusCode < 400:
      iconPath = mdiLinkVariant;
      break;
    case statusCode == 401:
      iconPath = mdiAccountQuestion;
      break;
    case statusCode == 403:
      iconPath = mdiAccountLock;
      break;
    case statusCode == 404:
      iconPath = mdiLinkOff;
      break;
    case statusCode == 451:
      iconPath = mdiScale;
      break;
    case statusCode >= 400 && statusCode < 500:
      iconPath = mdiExclamationThick;
      break;
    case statusCode >= 502 && statusCode <= 504:
      iconPath = mdiLanDisconnect;
      break;
    case statusCode == 507:
      iconPath = mdiHarddiskRemove;
      break;
    case statusCode == 508:
      iconPath = mdiReload;
      break;
    case statusCode >= 500 && statusCode < 600:
      iconPath = mdiBug;
      break;
  }

  return (
    <Site flexDirection="row">
      <Head>
        <title>{title}</title>
      </Head>
      <main className="flex flex-grow flex-col items-center place-content-center">
        <Icon path={iconPath} className="text-gray-500 w-48 h-48" />
        <div className="text-primary-500 text-4xl font-bold">
          {statusCode} {title}
        </div>
        <a
          href="/"
          className={cn(
            "p-2",
            "border-b-2",
            "text-gray-300",
            "select-none",
            "border-gray-500",
            "hover:border-primary-400",
          )}
        >
          {localization.returnHomeLabel}
        </a>
      </main>
    </Site>
  );
};
