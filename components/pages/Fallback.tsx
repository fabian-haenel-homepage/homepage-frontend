import React from "react";
import { WagtailPageBase } from "../../wagtail/pages/types";
import Icon from "@mdi/react";
import { mdiHammerWrench } from "@mdi/js";
import English from "../../i18n/wagtail/fallback/en.json";

export type FallbackFC = React.FC<WagtailPageBase>;

interface Localization {
  underConstructionLabel: string;
}

const i18n = (locale: string): Localization => {
  switch (locale) {
    case "en":
      return English;
    default:
      return English;
  }
};

export const Fallback: FallbackFC = () => {
  const localization = i18n("en"); // TODO use next.js localization feature

  return (
    <main className="flex flex-grow flex-col items-center place-content-center">
      <Icon path={mdiHammerWrench} className="text-gray-500 w-80 h-80 py-16" />
      <div className="text-primary-500 text-4xl font-bold">
        {localization.underConstructionLabel}
      </div>
    </main>
  );
};
