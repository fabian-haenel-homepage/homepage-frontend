import React from "react";
import Icon from "@mdi/react";
import { H3 } from "../../basic/H3";
import { mdiAccount, mdiEmail, mdiMap, mdiMapMarker, mdiPhone } from "@mdi/js";
import { useAlignment } from "../../basic/Alignment";
import { insert, cn } from "../../lib/classNames";
import { ClassNames } from "../../lib/types";

export interface ContactProps {
  fullName: string;
  street: string;
  houseNumber: string;
  postalCode: string;
  city: string;
  country: string;
  phoneNumber: string;
  email: string;
  className?: ClassNames;
}

interface DetailProps {
  iconPath: string;
  children?: string | string[];
}

const Detail: React.FC<DetailProps> = ({ iconPath, children }) => {
  const defaultAlignment = useAlignment();

  const classNames = ["flex", "items-center", "gap-2"];

  switch (defaultAlignment) {
    case "right":
      classNames.push("flex-row-reverse");
      break;
    case "left":
    default:
      classNames.push("flex-row");
      break;
  }

  return children ? (
    <div className={cn(classNames)}>
      <Icon path={iconPath} className="text-primary-500 w-8 h-8" />
      <H3>{children.toString()}</H3>
    </div>
  ) : null;
};

export const Contact: React.FC<ContactProps> = ({
  className,
  fullName,
  street,
  houseNumber,
  postalCode,
  city,
  country,
  phoneNumber,
  email,
}) => {
  const classNames = ["flex", "gap-1", "flex-col"];
  insert(classNames, className);
  return (
    <div className={cn(classNames)}>
      <Detail iconPath={mdiAccount}>{fullName}</Detail>
      <Detail iconPath={mdiMapMarker}>
        {street} {houseNumber}
      </Detail>
      <Detail iconPath={mdiMap}>
        {postalCode} {city} – {country}
      </Detail>
      <Detail iconPath={mdiPhone}>{phoneNumber}</Detail>
      <Detail iconPath={mdiEmail}>{email}</Detail>
    </div>
  );
};
