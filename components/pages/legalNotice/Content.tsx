import React from "react";
import {
  ContentBlock,
  ContentBlockType,
} from "../../../wagtail/pages/legalNotice/types";
import { Link } from "../../basic/Link";
import {
  H2 as H2Props,
  H3 as H3Props,
  Paragraph as ParagraphProps,
  Annotation as AnnotationProps,
  Link as LinkProps,
} from "../../../wagtail/pages/legalNotice/types";
import { Annotation } from "../../basic/Annotation";
import { Paragraph } from "../../basic/Paragraph";
import { H3 } from "../../basic/H3";
import { H2 } from "../../basic/H2";
import { ClassNames } from "../../lib/types";
import { insert, cn } from "../../lib/classNames";

export interface ContentProps {
  className?: ClassNames;
  blocks: ContentBlock[];
}

const H2Block: React.FC<H2Props> = ({ value }) => <H2>{value}</H2>;

const H3Block: React.FC<H3Props> = ({ value }) => <H3>{value}</H3>;

const AnnotationBlock: React.FC<AnnotationProps> = ({ value }) => (
  <Annotation>{value}</Annotation>
);

const ParagraphBlock: React.FC<ParagraphProps> = ({ value }) => (
  <Paragraph alignMobile="default">{value}</Paragraph>
);

const LinkBlock: React.FC<LinkProps> = ({ url, label }) => (
  <Link href={url}>{label}</Link>
);

const getBlockComponentForField = (field: ContentBlockType) => {
  switch (field) {
    case "H2":
      return H2Block;
    case "H3":
      return H3Block;
    case "Paragraph":
      return ParagraphBlock;
    case "Annotation":
      return AnnotationBlock;
    case "Link":
      return LinkBlock;
    default:
      // eslint-disable-next-line react/display-name
      return () => <></>;
  }
};

export const Content: React.FC<ContentProps> = ({ className, blocks }) => {
  const classNames = ["flex", "gap-4", "flex-col"];
  insert(classNames, className);
  return (
    <div className={cn(classNames)}>
      {blocks.map((block, index) => {
        const Block = getBlockComponentForField(block.field);
        // @ts-ignore
        return <Block key={index} {...block} />;
      })}
    </div>
  );
};
