import React from "react";
import { AboutMePage } from "../../wagtail/pages/aboutMe/types";
import { Description } from "./aboutMe/Description";
import { Experience } from "./aboutMe/Experience";
import {
  mdiBookOpenVariant,
  mdiCodeBraces,
  mdiSchool,
  mdiBookmarkMultiple,
  mdiAccountBoxMultiple,
} from "@mdi/js";
import { Education } from "./aboutMe/Education";
import { Technologies } from "./aboutMe/Technologies";

import English from "../../i18n/wagtail/aboutMe/en.json";
import { Language } from "./aboutMe/Language";
import { H1 } from "../basic/H1";
import { AlignmentContext } from "../basic/Alignment";
import { BlockList } from "../basic/BlockList";
import { Paragraph } from "../basic/Paragraph";
import { HRList } from "../basic/HRList";
import { cn } from "../lib/classNames";
import { Picture } from "../basic/Picture";
import { If } from "../basic/If";

export type AboutMeFC = React.FC<AboutMePage>;

interface Localization {
  descriptionLabel: string;
  experiencesLabel: string;
  educationLabel: string;
  technologiesLabel: string;
  languagesLabel: string;
}

const i18n = (locale: string): Localization => {
  switch (locale) {
    case "en":
      return English;
    default:
      return English;
  }
};

export const AboutMe: AboutMeFC = ({
  description,
  experiences,
  education,
  technologies,
  technologyComment,
  languages,
  titleImage,
}) => {
  const localization = i18n("en"); // TODO use next.js localization feature

  return (
    <main
      className={cn(
        "flex-grow",
        "flex",
        "flex-col",
        "gap-1",
        "self-center",
        "w-full",
        "lg:w-lg",
        "2xl:w-2xl",
        "bg-gray-800",
        "pb-8",
        "border-l",
        "border-r",
        "border-gray-700",
      )}
    >
      {titleImage ? (
        <Picture
          src={titleImage.url}
          width={titleImage.width}
          height={titleImage.height}
        />
      ) : null}

      <If condition={description}>
        <AlignmentContext alignment="left">
          <H1 iconPath={mdiAccountBoxMultiple}>
            {localization.descriptionLabel}
          </H1>
          <Description className="px-4 sm:px-8" blocks={description} />
        </AlignmentContext>
      </If>

      <If condition={experiences}>
        <AlignmentContext alignment="right">
          <H1 iconPath={mdiBookmarkMultiple}>
            {localization.experiencesLabel}
          </H1>
          <BlockList className="px-4 sm:px-8">
            {experiences.map((props, index) => (
              <Experience key={index} {...props} />
            ))}
          </BlockList>
        </AlignmentContext>
      </If>

      <If condition={education}>
        <AlignmentContext alignment="left">
          <H1 iconPath={mdiSchool}>{localization.educationLabel}</H1>
          <BlockList className="px-4 sm:px-8">
            {education.map((props, index) => (
              <Education key={index} {...props} />
            ))}
          </BlockList>
        </AlignmentContext>
      </If>

      <If condition={technologies.length || technologyComment}>
        <AlignmentContext alignment="right">
          <H1 iconPath={mdiCodeBraces}>{localization.technologiesLabel}</H1>
          <Paragraph className="px-4 sm:px-8">{technologyComment}</Paragraph>
          <Technologies className="px-4 sm:px-8 pt-4" items={technologies} />
        </AlignmentContext>
      </If>

      <If condition={languages}>
        <AlignmentContext alignment="left">
          <H1 iconPath={mdiBookOpenVariant}>{localization.languagesLabel}</H1>
          <HRList className="px-4 sm:px-8">
            {languages.map((props, index) => (
              <Language key={index} {...props} />
            ))}
          </HRList>
        </AlignmentContext>
      </If>
    </main>
  );
};
