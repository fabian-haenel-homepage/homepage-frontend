import React from "react";
import { mdiScaleBalance } from "@mdi/js";
import { LegalNoticePage } from "../../wagtail/pages/legalNotice/types";
import { cn } from "../lib/classNames";
import { AlignmentContext } from "../basic/Alignment";
import { H1 } from "../basic/H1";
import { Contact } from "./legalNotice/Contact";
import { Content } from "./legalNotice/Content";

export type LegalNoticeFC = React.FC<LegalNoticePage>;

export const LegalNotice: LegalNoticeFC = ({
  title,
  fullName,
  street,
  houseNumber,
  postalCode,
  city,
  country,
  phoneNumber,
  email,
  content,
}) => {
  return (
    <main
      className={cn(
        "flex-grow",
        "flex",
        "flex-col",
        "self-center",
        "w-full",
        "lg:w-lg",
        "2xl:w-2xl",
        "bg-gray-800",
        "pb-8",
        "border-l",
        "border-r",
        "border-gray-700",
      )}
    >
      <AlignmentContext alignment="left">
        <H1 iconPath={mdiScaleBalance}>{title}</H1>
        <Contact
          className="px-4 sm:px-8"
          {...{
            fullName,
            street,
            houseNumber,
            postalCode,
            city,
            country,
            phoneNumber,
            email,
          }}
        />
        <Content className="px-4 sm:px-8 mt-8" blocks={content} />
      </AlignmentContext>
    </main>
  );
};
