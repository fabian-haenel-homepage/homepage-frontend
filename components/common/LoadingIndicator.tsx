import React, { useEffect, useState } from "react";
import {
  mdiCheckboxBlankCircleOutline,
  mdiCircleSlice1,
  mdiCircleSlice2,
  mdiCircleSlice3,
  mdiCircleSlice4,
  mdiCircleSlice5,
  mdiCircleSlice6,
  mdiCircleSlice7,
  mdiCircleSlice8,
  mdiHexagonOutline,
  mdiHexagonSlice1,
  mdiHexagonSlice2,
  mdiHexagonSlice3,
  mdiHexagonSlice4,
  mdiHexagonSlice5,
  mdiHexagonSlice6,
} from "@mdi/js";
import Icon from "@mdi/react";
import { insert, cn } from "../lib/classNames";
import { ClassNames } from "../lib/types";

export type LoadingIndicatorSize =
  | "microscopic"
  | "tiny"
  | "small"
  | "medium"
  | "large"
  | "normal"
  | "custom";
export type LoadingIndicatorStyle = "hexagon" | "circle";

export interface LoadingIndicatorProps {
  size?: LoadingIndicatorSize;
  style?: LoadingIndicatorStyle;
  className?: ClassNames;
}

const CIRCLE_ICONS = [
  mdiCheckboxBlankCircleOutline,
  mdiCircleSlice1,
  mdiCircleSlice2,
  mdiCircleSlice3,
  mdiCircleSlice4,
  mdiCircleSlice5,
  mdiCircleSlice6,
  mdiCircleSlice7,
  mdiCircleSlice8,
];

const HEXAGON_ICONS = [
  mdiHexagonOutline,
  mdiHexagonSlice1,
  mdiHexagonSlice2,
  mdiHexagonSlice3,
  mdiHexagonSlice4,
  mdiHexagonSlice5,
  mdiHexagonSlice6,
];

export const LoadingIndicator: React.FC<LoadingIndicatorProps> = ({
  size = "normal",
  style = "hexagon",
  className = "",
}) => {
  const [index, setIndex] = useState(0);
  const classNames = ["text-primary-500"];
  let icons;

  switch (size) {
    case "custom":
      break;
    case "large":
      classNames.push("flex-none", "w-64", "h-64");
      break;
    case "medium":
      classNames.push("flex-none", "w-48", "h-48");
      break;
    case "small":
      classNames.push("flex-none", "w-24", "h-24");
      break;
    case "tiny":
      classNames.push("flex-none", "w-16", "h-16");
      break;
    case "microscopic":
      classNames.push("flex-none", "w-8", "w-8");
      break;
    case "normal":
    default:
      classNames.push("flex-none", "w-32", "h-32");
  }

  switch (style) {
    case "hexagon":
      icons = HEXAGON_ICONS;
      break;
    case "circle":
    default:
      icons = CIRCLE_ICONS;
  }
  insert(classNames, className);
  useEffect(() => {
    const interval = setInterval(
      () => setIndex((index + 1) % icons.length),
      500,
    );
    return () => clearInterval(interval);
  });

  return <Icon path={icons[index]} className={cn(classNames)} />;
};
