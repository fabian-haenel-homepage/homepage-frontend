import React, { useEffect, useState } from "react";
import { LoadingIndicator } from "./LoadingIndicator";
import { useRouter } from "next/router";
import { cn } from "../lib/classNames";
import English from "../../i18n/components/ChangePageIndicator/en.json";
import { sprintf } from "sprintf-js";
import { KnownRoutesMapping } from "../../wagtail/mapKnownRoutes";

interface Localization {
  label: string;
}

export interface ChangePageIndicatorProps {
  knownRoutes?: KnownRoutesMapping;
}

const i18n = (locale: string): Localization => {
  switch (locale) {
    case "en":
      return English;
    default:
      return English;
  }
};

export const ChangePageIndicator: React.FC<ChangePageIndicatorProps> = ({
  knownRoutes = {},
}) => {
  const [targetUrl, setTargetUrl] = useState<string | null>(null);
  const router = useRouter();

  const localization = i18n("en"); // TODO

  const urlString = targetUrl?.slice(1) ?? "";
  const targetString =
    urlString in knownRoutes ? knownRoutes[urlString] : urlString;

  useEffect(() => {
    const setChanging = (url: string) => setTargetUrl(url);
    const unsetChanging = () => setTargetUrl(null);

    router.events.on("routeChangeStart", setChanging);
    router.events.on("routeChangeComplete", unsetChanging);
    router.events.on("routeChangeError", unsetChanging);

    return () => {
      router.events.off("routeChangeStart", setChanging);
      router.events.off("routeChangeComplete", unsetChanging);
      router.events.off("routeChangeError", unsetChanging);
    };
  });

  return targetUrl ? (
    <div className="sticky w-full bottom-0">
      <div className="relative w-full">
        <div className="absolute w-full sm:w-auto -top-24 right-0">
          <div
            className={cn(
              "bg-gray-800",
              "flex",
              "flex-row",
              "items-center",
              "justify-center",
              "p-4",
              "m-4",
              "gap-4",
              "rounded",
              "border-gray-600",
              "border",
            )}
          >
            <LoadingIndicator size="microscopic" />
            <div className="text-gray-300 truncate">
              {sprintf(localization.label, targetString)}
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};
