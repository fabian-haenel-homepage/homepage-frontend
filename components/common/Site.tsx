import React from "react";
import { cn } from "../lib/classNames";

export type SiteFlexDirection = "row" | "row-reverse" | "col" | "col-reverse";

export interface SiteProps {
  flexDirection?: SiteFlexDirection;
  children?: React.ReactNode;
}

export const Site: React.FC<SiteProps> = ({
  flexDirection = "col",
  children,
}) => {
  const classNames = [
    "flex",
    "bg-gray-800",
    "w-full",
    "h-full",
    "min-h-screen",
    "min-w-screen",
  ];

  switch (flexDirection) {
    case "row":
      classNames.push("flex-row");
      break;
    case "row-reverse":
      classNames.push("flex-row-reverse");
      break;
    case "col-reverse":
      classNames.push("flex-col-reverse");
      break;
    case "col":
    default:
      classNames.push("flex-col");
      break;
  }
  return (
    <div
      className={cn(classNames)}
      style={{
        backgroundImage: 'url("/background-nerd.png")',
        backgroundRepeat: "repeat",
        backgroundSize: "192px 192px",
      }}
    >
      {children}
    </div>
  );
};
