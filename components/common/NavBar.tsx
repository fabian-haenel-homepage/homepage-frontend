import React, { useEffect, useState } from "react";
import { Navigation } from "../../wagtail/navigation/types";
import Icon from "@mdi/react";
import { mdiClose, mdiMenu, mdiSkull } from "@mdi/js";
import Link from "next/link";
import { cn } from "../lib/classNames";
import { useRouter } from "next/router";

export interface NavBarProps extends Navigation {
  selectedSlugs: string[];
}

interface NavItemProps {
  title: string;
  urlPath: string;
  selectedSlugs: string[];
}

const DesktopNavItem: React.FC<NavItemProps> = ({
  title,
  urlPath,
  selectedSlugs,
}) => {
  return (
    <li>
      <Link href={urlPath}>
        <a
          className={cn(
            "p-2",
            "border-b-2",
            "inline-flex",
            "text-gray-300",
            "select-none",
            selectedSlugs.join("/") == urlPath
              ? "border-primary-500"
              : "border-gray-500 hover:border-primary-500",
          )}
        >
          {title}
        </a>
      </Link>
    </li>
  );
};

const MobileNavItem: React.FC<NavItemProps> = ({
  title,
  urlPath,
  selectedSlugs,
}) => {
  return (
    <li className={"bg-gray-800"}>
      <Link href={urlPath}>
        <a
          className={cn(
            "px-2",
            "py-4",
            "inline-block",
            "border-b",
            "border-gray-700",
            "select-none",
            "w-full",
            "h-full",
            "text-center",
            "text-lg",
            selectedSlugs.join("/") == urlPath
              ? "text-primary-500"
              : "text-gray-300 hover:text-primary-500",
          )}
        >
          {title}
        </a>
      </Link>
    </li>
  );
};

export const NavBar: React.FC<NavBarProps> = ({
  siteName,
  selectedSlugs,
  items,
}) => {
  const router = useRouter();
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  useEffect(() => {
    const handleRouterChangeStart = () => setIsMenuOpen(false);
    router.events.on("routeChangeStart", handleRouterChangeStart);
    return () => {
      router.events.off("routeChangeStart", handleRouterChangeStart);
    };
  });

  return (
    <nav>
      <div
        className={cn(
          "flex",
          "flex-grow-0",
          "items-center",
          "gap-4",
          "bg-gray-800",
          "border-b",
          "border-gray-700",
        )}
      >
        <Link href="/">
          <a className="content-center flex flex-row items-center flex-grow-0 gap-2 w-64">
            <Icon
              path={mdiSkull}
              className={cn(
                "flex-none",
                "w-12",
                "h-12",
                "m-2",
                "p-1",
                "content-center",
                "rounded-full",
                "bg-primary-500",
                "text-gray-300",
              )}
            />
            <span className="text-gray-300 text-2xl font-medium select-none">
              {siteName}
            </span>
          </a>
        </Link>

        {/* Spacer */}
        <div className="flex-grow" />

        {/* ----- Mobile ----- */}

        <button
          className="block sm:hidden flex-none"
          onClick={() => setIsMenuOpen(!isMenuOpen)}
        >
          <Icon
            path={isMenuOpen ? mdiClose : mdiMenu}
            className={cn(
              "flex-none",
              "w-12",
              "h-12",
              "m-2",
              "p-1",
              "content-center",
              "text-gray-300",
            )}
          />
        </button>

        {/* ----- Mobile end ----- */}

        {/* ----- Desktop ----- */}

        {/* Navigation items */}
        <ul className="hidden sm:flex flex-grow-0 flex-row gap-2">
          {items.map(({ title, id, urlPath }) => (
            <DesktopNavItem
              key={id}
              title={title}
              urlPath={urlPath}
              selectedSlugs={selectedSlugs}
            />
          ))}
        </ul>

        {/* Spacer */}
        <div className="hidden sm:block flex-grow" />

        {/* Dummy as counter to the home icon */}
        <div className="hidden sm:block w-64" />

        {/* ----- Desktop end ----- */}
      </div>

      <ul className={isMenuOpen ? "block absolute w-full z-50" : "hidden"}>
        {items.map(({ title, id, urlPath }) => (
          <MobileNavItem
            key={id}
            title={title}
            urlPath={urlPath}
            selectedSlugs={selectedSlugs}
          />
        ))}
      </ul>
    </nav>
  );
};
