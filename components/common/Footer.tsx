import React from "react";
import { Footer as FooterProps } from "../../wagtail/footer/types";
import Link from "next/link";
import { H5 } from "../basic/H5";
import Icon from "@mdi/react";
import {
  mdiGraphql,
  mdiLanguagePython,
  mdiLanguageTypescript,
  mdiReact,
  mdiTailwind,
  mdiGitlab,
} from "@mdi/js";
import English from "../../i18n/footer/en.json";
import { mdiWagtail } from "../../lib/icons";
import { cn } from "../lib/classNames";
import { NextJSLogo } from "../icons/NextJSLogo";
import { HR } from "../basic/HR";

interface Localization {
  legalNoticeLabel: string;
  buildWithLabel: string;
  sourceCodeLabel: string;
}

const BUILD_WITH_LOGOS = [
  [mdiLanguagePython, "https://www.python.org/"],
  [mdiLanguageTypescript, "https://www.typescriptlang.org/"],
  [mdiReact, "https://reactjs.org/"],
  [mdiTailwind, "https://tailwindcss.com/"],
  [mdiGraphql, "https://graphql.org/"],
  [mdiWagtail, "https://wagtail.io/"],
];

const i18n = (locale: string): Localization => {
  switch (locale) {
    case "en":
      return English;
    default:
      return English;
  }
};

export const Footer: React.FC<FooterProps> = ({ pages }) => {
  const { buildWithLabel, legalNoticeLabel, sourceCodeLabel } = i18n("en");

  return (
    <footer
      className={cn(
        "flex",
        "flex-grow-0",
        "flex-col-reverse",
        "items-center",
        "justify-center",
        "gap-4",
        "sm:flex-row",
        "sm:items-start",
        "sm:gap-16",
        "bg-gray-800",
        "border-t",
        "border-gray-700",
        "px-4",
        "py-8",
        "sm:p-8",
      )}
    >
      <div className="flex flex-col flex-grow-0 items-center">
        <H5 className="pb-2">{sourceCodeLabel}</H5>
        <ul className="flex flex-row justify-center flex-wrap gap-2">
          <li>
            <a href={process.env.NEXT_PUBLIC_REPOSITORY_URL}>
              <Icon
                path={mdiGitlab}
                className="text-gray-300 w-8 h-8 sm:w-12 sm:h-12"
              />
            </a>
          </li>
        </ul>
      </div>

      {/* List of technologies with which this webpage was build */}
      <div className="flex flex-col flex-grow-0 items-center">
        <H5 className="pb-2">{buildWithLabel}</H5>
        <ul className="flex flex-row justify-center flex-wrap gap-2">
          {BUILD_WITH_LOGOS.map(([iconPath, url], index) => (
            <li key={index}>
              <a href={url}>
                <Icon
                  path={iconPath}
                  className="text-gray-300 w-8 h-8 sm:w-12 sm:h-12"
                />
              </a>
            </li>
          ))}
          <li>
            <a href="https://nextjs.org">
              <NextJSLogo className="text-gray-300 h-8 sm:h-12" />
            </a>
          </li>
        </ul>
      </div>

      <HR className="block sm:hidden" />

      {/* Any legal links */}
      <div className="flex-none">
        <H5 className="pb-2">{legalNoticeLabel}</H5>
        <ul className="flex flex-col items-center">
          {pages.map(({ id, urlPath, title }) => (
            <li key={id}>
              <Link href={urlPath}>
                <a className="text-gray-300">{title}</a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </footer>
  );
};
