FROM node:16-buster-slim

# Disable nextjs telemetry by default
ARG NEXT_TELEMETRY_DISABLED=1
ENV NEXT_TELEMETRY_DISABLED=$NEXT_TELEMETRY_DISABLED

ARG YARN_CACHE_FOLDER="/packages/.yarn/cache"
ENV YARN_CACHE_FOLDER=$YARN_CACHE_FOLDER

WORKDIR /packages

COPY .yarn/releases ./.yarn/releases

COPY .yarnrc.yml package.json yarn.lock ./

RUN yarn install --immutable
