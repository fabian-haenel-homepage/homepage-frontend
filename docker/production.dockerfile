FROM node:16-buster-slim as base

# Disable nextjs telemetry by default
ARG NEXT_TELEMETRY_DISABLED=1
ENV NEXT_TELEMETRY_DISABLED=$NEXT_TELEMETRY_DISABLED

# Set node environment to production
ARG NODE_ENV="production"
ENV NODE_ENV=$NODE_ENV

ARG CI_REPOSITORY_URL
ARG NEXT_PUBLIC_REPOSITORY_URL=$CI_REPOSITORY_URL
ENV NEXT_PUBLIC_REPOSITORY_URL=$CI_REPOSITORY_URL

WORKDIR /app

COPY .yarn/releases ./.yarn/releases

COPY .yarnrc.yml package.json yarn.lock ./

RUN yarn install --immutable

FROM base as build

COPY next.config.js \
    next-env.d.ts \
    postcss.config.js \
    tailwind.config.js \
    tsconfig.json \
    .eslintrc.json \
    ./

COPY components ./components
COPY i18n ./i18n
COPY lib ./lib
COPY pages ./pages
COPY public ./public
COPY wagtail ./wagtail

RUN yarn build

FROM base

# Setup docker entrypoint.
COPY docker/docker-entrypoint.sh /
COPY docker/docker-entrypoint.d /docker-entrypoint.d/

RUN chmod +x /docker-entrypoint.sh \
        /docker-entrypoint.d/*.sh

ENTRYPOINT ["/docker-entrypoint.sh"]

# Copy required files from build stage
COPY next.config.js next-env.d.ts ./
COPY --from=build /app/.next ./.next
COPY --from=build /app/public ./public

CMD ["yarn", "start"]

EXPOSE 3000

ARG VERSION
ENV VERSION=$VERSION
