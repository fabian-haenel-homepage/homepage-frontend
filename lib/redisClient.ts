import { createClient } from "redis";
import { serverConfiguration } from "./configuration";
import { IS_BUILDING } from "./common";

const redisClient = createClient({ url: serverConfiguration.redisUrl });

redisClient.on("error", (err) => console.log("Redis Client Error", err));

if (!IS_BUILDING) {
  redisClient
    .connect()
    .then(() => console.info("redisClient", "Successfully connected"))
    .catch((reason) =>
      console.log("redisClient", "Raised exception while connecting", reason),
    );
}

export default redisClient;
