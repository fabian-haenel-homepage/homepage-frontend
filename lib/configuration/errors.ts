export class ConfigurationError extends Error {}

export class ServerSideSettingAccessedByClient extends ConfigurationError {
  constructor(public settingName: string) {
    super(
      `${settingName}: Server side setting is being accessed from client side code`,
    );
  }
}

export class ParsingError extends ConfigurationError {
  constructor(public settingName: string, message: string) {
    super(`${settingName}: ${message}`);
  }
}

export class MissingRequiredSetting extends ParsingError {
  constructor(public settingName: string) {
    super(settingName, "Required setting is not configured");
  }
}

export class FailedValidation extends ParsingError {
  constructor(public settingName: string, public details: string) {
    super(settingName, `Failed validation - ${details}`);
  }
}
