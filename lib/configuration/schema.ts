import { ParserFunction } from "./parsing";
import { ServerSideSettingAccessedByClient } from "./errors";

export type SchemaField<T> = [
  parser: ParserFunction<T>,
  buildValue: T,
  fallback?: T,
];

export type NextRuntimeConfig = {
  [setting: string]: any;
};

export type Configuration<T> = {
  [setting in keyof T]?: T[setting];
};

export type ConfigurationSchema<T> = {
  [setting in keyof T]: SchemaField<T[setting]>;
};

export function initializeConfigurationFromSchema<T>(
  schema: ConfigurationSchema<T>,
  source: NextRuntimeConfig,
  accessibleByClient: boolean = false,
): T {
  source = source ?? {};
  const configuration: Configuration<T> = {};
  for (const setting in schema) {
    if (process.browser && !accessibleByClient) {
      Object.defineProperty(configuration, setting, {
        get: (settingName: string = setting) => {
          throw new ServerSideSettingAccessedByClient(settingName);
        },
      });
    } else {
      const [parser, buildValue, fallback] = schema[setting];
      configuration[setting] = parser(
        setting,
        source[setting],
        buildValue,
        fallback,
      );
    }
  }
  return <T>configuration;
}
