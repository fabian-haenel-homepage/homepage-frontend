import { FailedValidation, MissingRequiredSetting } from "./errors";
import { IS_BUILDING } from "../common";

export type Converter<T> = (raw: string) => T;

export type Validator<T> = (value: T) => string | null;

export type ParserFunction<T> = (
  settingName: string,
  raw: string | undefined,
  buildValue: T,
  fallback?: T,
) => T;

export function createParser<T>(
  parser: Converter<T>,
  validator: Validator<T> = (_) => null,
  defaultFallback?: T,
): ParserFunction<T> {
  return (settingName, raw, buildValue, fallback = defaultFallback) => {
    if (IS_BUILDING) {
      return buildValue;
    }

    if (raw === undefined) {
      if (fallback === undefined) throw new MissingRequiredSetting(settingName);
      return fallback;
    }

    const value = parser(raw);
    const errorMessage = validator(value);
    if (errorMessage !== null) {
      throw new FailedValidation(settingName, errorMessage);
    }

    return value;
  };
}

export function extendParser<T>(
  parser: ParserFunction<T>,
  defaultFallback: T | undefined,
  ...validators: Validator<T>[]
): ParserFunction<T> {
  return (settingName, raw, buildValue, fallback = defaultFallback) => {
    const value = parser(settingName, raw, buildValue, fallback);
    for (const validator of validators) {
      const errorMessage = validator(value);
      if (errorMessage !== null) {
        throw new FailedValidation(settingName, errorMessage);
      }
    }
    return value;
  };
}

export function createUrlParser(
  validProtocol: string,
  ...validProtocols: string[]
): ParserFunction<string> {
  validProtocols.unshift(validProtocol);
  validProtocols = validProtocols.map((protocol) => protocol.toUpperCase());
  return createParser<string>(
    (raw) => raw,
    (value) => {
      try {
        const url = new URL(value);
        const protocol = url.protocol.slice(0, -1).toUpperCase();
        return validProtocols.some((schema) => schema === protocol)
          ? null
          : `Provided URL does not match required protocols ${validProtocols.join(
              " | ",
            )}`;
      } catch {
        return "Provided value is not a valid URL";
      }
    },
  );
}

export const integer = createParser<number>(parseInt, (value) =>
  isNaN(value) ? "Provided value is not a number" : null,
);

export const positiveIntegerLargerThanZero = extendParser(
  integer,
  undefined,
  (value) => (value <= 0 ? "Provided value must be larger than 0" : null),
);

export const httpUrl = createUrlParser("http", "https");

export const wsUrl = createUrlParser("ws", "wss");

export const redisUrl = createUrlParser("redis", "rediss");
