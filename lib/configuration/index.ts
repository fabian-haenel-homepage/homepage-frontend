import getConfig from "next/config";
import {
  httpUrl,
  integer,
  positiveIntegerLargerThanZero,
  redisUrl,
  wsUrl,
} from "./parsing";
import {
  ConfigurationSchema,
  initializeConfigurationFromSchema,
} from "./schema";

export interface PublicConfiguration {
  wagtailBaseUrl: string;
  wagtailWebsocketBaseUrl: string;
  wagtailSiteId: number;
}

export interface ServerConfiguration {
  pageTTL: number;
  navigationTTL: number;
  footerTTL: number;
  redisUrl: string;
}

const { publicRuntimeConfig, serverRuntimeConfig } = getConfig();

const PublicConfigurationSchema: ConfigurationSchema<PublicConfiguration> = {
  wagtailBaseUrl: [httpUrl, "http://127.0.0.1:8000"],
  wagtailWebsocketBaseUrl: [wsUrl, "ws://127.0.0.1:8000"],
  wagtailSiteId: [integer, 0],
};

const ServerConfigurationSchema: ConfigurationSchema<ServerConfiguration> = {
  footerTTL: [positiveIntegerLargerThanZero, 1, 300],
  navigationTTL: [positiveIntegerLargerThanZero, 1, 300],
  pageTTL: [positiveIntegerLargerThanZero, 1, 300],
  redisUrl: [redisUrl, "redis://127.0.0.1:6379"],
};

export const publicConfiguration = initializeConfigurationFromSchema(
  PublicConfigurationSchema,
  publicRuntimeConfig,
  true,
);
export const serverConfiguration = initializeConfigurationFromSchema(
  ServerConfigurationSchema,
  serverRuntimeConfig,
);
