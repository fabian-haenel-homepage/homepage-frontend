import "tailwindcss/tailwind.css";

import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import { Site } from "../components/common/Site";
import Head from "next/head";
import React from "react";
import { LoadingIndicator } from "../components/common/LoadingIndicator";
import English from "../i18n/_app/en.json";

interface Localization {
  loadingTitle: string;
}

const i18n = (locale: string): Localization => {
  switch (locale) {
    case "en":
      return English;
    default:
      return English;
  }
};

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();

  const localization = i18n("en");

  // Shows loading page during loading, also prevents issues during build with ISR
  if (router.isFallback) {
    return (
      <Site flexDirection="row">
        <Head>
          <title>{localization.loadingTitle}</title>
        </Head>
        <main className="flex justify-center w-full self-center">
          <LoadingIndicator size="large" style="hexagon" />
        </main>
      </Site>
    );
  }
  return <Component {...pageProps} />;
}
export default MyApp;
