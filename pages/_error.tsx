import React from "react";
import { NextPage, NextPageContext } from "next";
import { ErrorPage, ErrorProps } from "../components/pages/ErrorPage";

const _ErrorPage: NextPage<ErrorProps> = ({ statusCode = 500 }) => {
  return <ErrorPage statusCode={statusCode} />;
};

_ErrorPage.getInitialProps = ({ res, err }: NextPageContext): ErrorProps => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default _ErrorPage;
