import React from "react";
import { NextPage } from "next";
import { ErrorPage, ErrorProps } from "../components/pages/ErrorPage";

const _ErrorPage: NextPage<ErrorProps> = () => {
  return <ErrorPage statusCode={404} />;
};

export default _ErrorPage;
