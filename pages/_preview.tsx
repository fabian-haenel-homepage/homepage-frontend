import type { GetServerSideProps, NextPage } from "next";
import Page, { PageProps } from "./[[...slugs]]";
import { NotFound } from "../wagtail/errors";
import { resolveNavigationRepresentation } from "../wagtail/navigation/resolver";
import { resolveFooterRepresentation } from "../wagtail/footer/resolver";
import { ParsedUrlQuery } from "querystring";
import { resolvePagePreview } from "../wagtail/pages/resolver";
import { useState } from "react";

export interface PreviewPagesParsedUrlQuery extends ParsedUrlQuery {
  contentType: string;
  token: string;
}

export interface PreviewPageProps {
  pageProps: PageProps;
  contentType: string;
  token: string;
}

export const getServerSideProps: GetServerSideProps<
  PreviewPageProps,
  PreviewPagesParsedUrlQuery
> = async ({ query }) => {
  if (
    !(
      "content_type" in query &&
      typeof query.content_type === "string" &&
      "token" in query &&
      typeof query.token === "string"
    )
  ) {
    return {
      notFound: true,
    };
  }

  try {
    const [navigationData, footer, pageData] = await Promise.all([
      resolveNavigationRepresentation(),
      resolveFooterRepresentation(),
      resolvePagePreview(query.content_type, query.token),
    ]);

    return pageData
      ? {
          props: {
            pageProps: {
              navigation: navigationData,
              footer,
              page: pageData,
              currentSlugs: [], // TODO determine slugs
            },
            token: query.token,
            contentType: query.content_type,
          },
        }
      : {
          notFound: true,
        };
  } catch (e) {
    if (e instanceof NotFound) {
      return {
        notFound: true,
      };
    }

    throw e;
  }
};

const PreviewPage: NextPage<PreviewPageProps> = (props) => {
  const [pageProps, setPageProps] = useState<PageProps>(props.pageProps);

  // TODO use subscription to refresh
  // const _ = usePreviewSubscription(
  //   props.contentType,
  //   props.token,
  //   (options) =>
  //     options.subscriptionData.data &&
  //     setPageProps({
  //       navigation: pageProps.navigation,
  //       footer: pageProps.footer,
  //       page: options.subscriptionData.data.page,
  //       currentSlugs: [], // TODO determine slugs
  //     }),
  // );

  return <Page {...pageProps} />;
};

export default PreviewPage;
