import type { GetStaticPaths, GetStaticProps, NextPage } from "next";
import Head from "next/head";
import { NavBar } from "../components/common/NavBar";
import { Navigation } from "../wagtail/navigation/types";
import { ParsedUrlQuery } from "querystring";
import { Footer } from "../components/common/Footer";
import { Footer as FooterRepresentation } from "../wagtail/footer/types";
import React from "react";
import { WagtailPage as WagtailPageProps } from "../wagtail/pages/types";
import { resolveNavigationRepresentation } from "../wagtail/navigation/resolver";
import { resolvePage } from "../wagtail/pages/resolver";
import { WagtailPage } from "../components/pages/WagtailPage";
import { Site } from "../components/common/Site";
import { serverConfiguration } from "../lib/configuration";
import { resolveFooterRepresentation } from "../wagtail/footer/resolver";
import { NotFound } from "../wagtail/errors";
import { ChangePageIndicator } from "../components/common/ChangePageIndicator";
import { mapKnownRoutes } from "../wagtail/mapKnownRoutes";

export interface PageProps {
  navigation: Navigation;
  footer: FooterRepresentation;
  page: WagtailPageProps;
  currentSlugs: string[];
}

export interface PagesParsedUrlQuery extends ParsedUrlQuery {
  slugs: string[];
}

export const getStaticProps: GetStaticProps<PageProps, PagesParsedUrlQuery> =
  async ({ params }) => {
    if (!params) {
      return {
        notFound: true,
        revalidate: serverConfiguration.pageTTL,
      };
    }

    const slugs = "slugs" in params ? params.slugs : [];

    try {
      const [navigationData, footer, pageData] = await Promise.all([
        resolveNavigationRepresentation(),
        resolveFooterRepresentation(),
        resolvePage(slugs),
      ]);

      return pageData
        ? {
            props: {
              navigation: navigationData,
              footer,
              page: pageData,
              currentSlugs: slugs,
              timestamp: Date.now(),
            },
            revalidate: serverConfiguration.pageTTL,
          }
        : {
            notFound: true,
            revalidate: serverConfiguration.pageTTL,
          };
    } catch (e) {
      if (e instanceof NotFound) {
        return {
          notFound: true,
          revalidate: serverConfiguration.pageTTL,
        };
      }

      throw e;
    }
  };

export const getStaticPaths: GetStaticPaths = () => ({
  paths: [],
  fallback: true,
});

const Page: NextPage<PageProps> = ({
  navigation,
  currentSlugs,
  page,
  footer,
}) => {
  return (
    <Site>
      <Head>
        <title>
          {navigation.siteName} &#8210; {page.title}
        </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <NavBar selectedSlugs={currentSlugs} {...navigation} />

      <WagtailPage {...page} />

      <ChangePageIndicator
        knownRoutes={mapKnownRoutes({ navigation, footer, page })}
      />

      <Footer {...footer} />
    </Site>
  );
};

export default Page;
